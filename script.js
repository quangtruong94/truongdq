var head = document.getElementsByTagName('head')[0];
var link = document.createElement('link');
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = 'style.css';
head.appendChild(link);

var BOSH_SERVICE = "https://rd.zamba.vn:8866/http-bind/";
var domain = "chatzamba";
var project_name = "test_project";
var connection = null, msg_num;

if (readCookie("msgNum_" + readCookie("userChatID")) && readCookie("msgNum_" + readCookie("userChatID")) !== "") {
    msg_num = readCookie("msgNum_" + readCookie("userChatID"));
} else {
    msg_num = 0;
}

(function () {
    this.SupportChatBox = function () {
        var defaults = {
            connectFirst: true,
            headerColor: "#2f3755",
            headerTitle: "Tư vấn - hỗ trợ",
            greeting: "Em chào anh/chị. Anh/chị có đang tìm hiểu hoặc phân vân dự án nào không ạ? <br /> Em có thể tư vấn giúp anh chị."
        }

        // Create options by extending defaults with the passed in arugments
        if (arguments[0] && typeof arguments[0] === "object") {
            this.options = extendDefaults(defaults, arguments[0]);
        }
    }

    SupportChatBox.prototype.init = function () {
        syncTab.call(this);
        buildChatBox.call(this);
        toggleShowHide(this);
        eventListener.call(this);
    }

    function show() {
        writeCookie("boxStatus", 1);
        document.getElementsByClassName("chat-box")[0].setAttribute("class", "chat-box show");
        document.getElementsByClassName("header-title")[0].style.margin = "0";
        document.getElementsByClassName("send-to-email")[0].innerHTML = "&#9993";
        document.getElementsByClassName("show-history")[0].innerHTML = "&#128336";
        if (readCookie("supportName")) {
            document.getElementsByClassName("header-title")[0].getElementsByTagName("span")[0].innerHTML = readCookie("supportName");
        } else {
            document.getElementsByClassName("online-icon")[0].setAttribute("class", "online-icon offline");
            document.getElementsByClassName("header-title")[0].getElementsByTagName("span")[0].innerHTML = "Tư vấn" +
                " viên đang offline";
        }
        toggleShowHide();
    }

    function hide() {
        writeCookie("boxStatus", 0);
        document.getElementsByClassName("chat-box")[0].setAttribute("class", "chat-box hide");
        document.getElementsByClassName("header-title")[0].style.margin = "0 auto";
        if (readCookie("supportName") && readCookie("supportName") !== "") {
            document.getElementsByClassName("header-title")[0].getElementsByTagName("span")[0].innerHTML = "Tư vấn" +
                " đang online";
        } else {
            document.getElementsByClassName("header-title")[0].getElementsByTagName("span")[0].innerHTML = "Tư" +
                " vấn viên đang offline";
        }
        toggleShowHide();
    }

    function sendMsg(e) {
        createMessage("right", e, false, false, "", true);
        document.getElementsByClassName("input-chat")[0].value = "";
    }

    function buildChatBox() {
        var docFrag, onlineStatus;
        var chatBox = document.createElement("div");
        var headerChat = document.createElement("div");
        var headerTitle = document.createElement("div");
        var statusIcon = document.createElement("div");
        var supportPosition = document.createElement("span");
        var noti = document.createElement("span");
        var sendToEmail = document.createElement("div");
        var showHistory = document.createElement("div");
        var closeButton = document.createElement("div");
        var bodyChat = document.createElement("div");
        var overlay = document.createElement("div");
        var footerChat = document.createElement("div");
        var inputChat = document.createElement("textarea");
        var sendImage = document.createElement("label");
        var imgUpload = document.createElement("input");


        docFrag = document.createDocumentFragment();

        /* build header */
        headerChat.setAttribute("class", "header-chat");

        headerTitle.setAttribute("class", "header-title");
        noti.setAttribute("class", "notify");
        supportPosition.setAttribute("class", "support-position");
        supportPosition.innerHTML = "Tư vấn viên";

        onlineStatus = "offline";
        if (onlineStatus === "online") {
            statusIcon.setAttribute("class", "online-icon online");
        } else {
            statusIcon.setAttribute("class", "online-icon offline");
        }
        headerTitle.appendChild(statusIcon);
        headerTitle.insertAdjacentHTML("beforeend", "<span>" + this.options.headerTitle + "</span>");
        headerTitle.appendChild(supportPosition);
        headerChat.style.backgroundColor = this.options.headerColor;

        sendToEmail.setAttribute("class", "send-to-email");
        showHistory.setAttribute("class", "show-history");
        closeButton.setAttribute("class", "close-button");

        headerChat.appendChild(headerTitle);
        headerChat.appendChild(closeButton);
        headerChat.appendChild(showHistory);
        headerChat.appendChild(sendToEmail);
        headerChat.appendChild(noti);

        /* build body */
        bodyChat.setAttribute("class", "body-chat");
        overlay.setAttribute("class", "overlay");
        overlay.insertAdjacentHTML("beforeend", "<img src='puff.svg'>");
        bodyChat.appendChild(overlay);

        /* build footer */
        footerChat.setAttribute("class", "footer-chat");

        inputChat.setAttribute("class", "input-chat");
        inputChat.rows = 3;
        inputChat.setAttribute("placeholder", "Nhập nội dung");

        sendImage.setAttribute("for", "img-upload");
        sendImage.setAttribute("class", "send-image");

        imgUpload.setAttribute("id", "img-upload");
        imgUpload.setAttribute("type", "file");

        footerChat.appendChild(inputChat);
        footerChat.appendChild(sendImage);
        footerChat.appendChild(imgUpload);

        /* build Chat box */
        chatBox.setAttribute("class", "chat-box");

        if (this.options.connectFirst) {
            chatBox.setAttribute("class", "chat-box show");
            closeButton.innerHTML = "&#10092";
            sendToEmail.innerHTML = "&#9993";
            showHistory.innerHTML = "&#128336";
            writeCookie("boxStatus", 1);
            connectServer(this);
        } else {
            chatBox.setAttribute("class", "chat-box hide");
            sendImage.style.display = "none";
            closeButton.innerHTML = "&#10092";
            writeCookie("boxStatus", 0);
            connectServer(this);
        }

        chatBox.appendChild(headerChat);
        chatBox.appendChild(bodyChat);
        chatBox.appendChild(footerChat);

        docFrag.appendChild(chatBox);
        document.body.appendChild(docFrag);
    }

    function toggleShowHide() {
        // sự kiện click header để show, hide chat box
        document.getElementsByClassName("header-chat")[0].onclick = function () {
            if (document.getElementsByClassName("show")[0]) {
                hide();
            } else {
                show();
            }

        };
    }

    function syncTab() {
        window.onfocus = function () {
            if (readCookie("boxStatus")) {
                if (readCookie("boxStatus") === "1") {
                    if (readCookie("isUpdated")) {
                        if (readCookie("isUpdated") === "1") {
                            clearBodyChat();
                            getSupport();
                        }
                        if (readCookie("isUpdated") === "2") {
                            clearBodyChat();
                            getHistory(readCookie("supportJID"), readCookie("supportAva"));
                        }
                        writeCookie("isUpdated", 0);
                    }
                    show();
                } else if (readCookie("boxStatus") === "0") {
                    if (readCookie("isUpdated")) {
                        if (readCookie("isUpdated") === "1") {
                            clearBodyChat();
                            getSupport();
                        }
                        if (readCookie("isUpdated") === "2") {
                            clearBodyChat();
                            getHistory(readCookie("supportJID"), readCookie("supportAva"));
                        }
                        writeCookie("isUpdated", 0);
                    }
                    hide();
                }
            }
        }

        function clearBodyChat() {
            var bodyChat = document.getElementsByClassName("body-chat")[0];
            while (!bodyChat.lastChild.classList.contains("overlay")) {
                bodyChat.removeChild(bodyChat.lastChild);
            }
        }
    }

    var chatId = "", token = "";

    /* các sự kiện */
    function eventListener() {
        document.getElementsByClassName("input-chat")[0].addEventListener("keyup", function (e) {
            if (e.which === 13 && document.getElementsByClassName("input-chat")[0].value.trim() !== "") {
                e.preventDefault();
                connection.send(buildMessage("").message);
                //connection.send(buildMessage("").forwarded);
                writeCookie("isUpdated", 2);
                sendMsg(document.getElementsByClassName("input-chat")[0].value);
            }
        });

        document.getElementById("img-upload").addEventListener("change", function () {
            var fileExt = this.value.slice((this.value.lastIndexOf(".") - 1 >>> 0) + 2);
            var img = this.files[0];
            if (fileExt !== "jpg" && fileExt !== "png" && fileExt !== "gif" && fileExt !== "jpeg") {
                createMessage("", "File bạn chọn không phải là hình ảnh", false, true, "", true);
                return;
            } else if (img.size > 5242880) {
                createMessage("", "Dung lượng ảnh không vượt quá 5MB", false, true, "", true);
                return;
            }

            var formData = new FormData();
            formData.append("image", img);

            callApi("http://chat.zamba.vn/ufile/ajaxupload.php", formData, "POST", "", function (response) {
                connection.send(buildMessage(response).message);
                //connection.send(buildMessage(response).forwarded);
                writeCookie("isUpdated", 2);
                sendMsg(response);
            }, function () {
                console.log("Fail to upload image");
                createMessage("", "Ảnh chưa được upload", false, true, "", true);
            });
        });
    }

    function buildMessage(img) {
        if (readCookie("connected") && readCookie("connected") === "1") {
            var to = "";
            if (document.getElementsByClassName("header-title")[0].getElementsByTagName("input")[0] !== undefined) {
                to = document.getElementsByClassName("header-title")[0].getElementsByTagName("input")[0].value
                    + "@" + domain;
            } else {
                return;
            }

            var from = readCookie("userChatID") + "@" + domain;
            var msg;
            if (img === "") {
                msg = document.getElementsByClassName("input-chat")[0].value;
            } else {
                msg = img;
            }

            var mesId = Date.now();

            var buildMessage = {
                "toemail": "quangtruong16994@gmail.com",
                "tousername": "Đỗ Quang Trường",
                "tofid": to,
                "fromemail": chatId,
                "fromid": chatId,
                "msg": msg,
                "mesid": mesId,
                "images": "",
                "fromusername": "",
                "fromname": "Khách hàng"
            };

            var infor = {
                "location": window.location.href,
                "user_agent": navigator.userAgent,
                "app_name": navigator.appName,
                "url": window.location.href,
                "title": document.title,
                "ip": "Đang cập nhật"
            }

            var message = $msg({from: from, to: to, type: "chat", id: mesId})
                .c('body').t(JSON.stringify(buildMessage)).up()
                .c('fullname').t("Đỗ Quang Trường").up()
                .c('email').t(chatId).up()
                .c('avatar').t("https://rdapi.zamba.vn/static/avatar/giangnguyenthuy_vccorp_vn.jpg").up()
                .c('info').t(JSON.stringify(infor)).up()
                .c('status').t('434990C8A25D2BE94863561AE98BD682').up()
                .c('project').t(project_name).up()
                .c('referrer').t("").up()
                .c('url_referrer').t("").up()
                .c('wellcome').t("").up()
                .c('active', {xmlns: 'http://jabber.org/protocal/chatstates'});

            /*            var forwarded = $msg({to: from, type: 'chat', id: mesId})
             .c("forwarded", {xmlns: "urn.xmpp.forward:0"})
             .c("delay", {xmns: "urn:xmpp:delay", stamp: mesId}).up()
             .cnode(message.tree());*/

            return {"message": message};
        } else {
            createMessage("left", "Chưa kết nối được đến server", false, true, "", true);
        }
    }

    function createUser() {
        var truth_code = "102ee893a4ce9e663287e79619d6e2d5", lie_code = "chatzamba", username = new Date().getTime();
        var email = "", password = "", name = "", project = project_name, type = 0, user_id = "", init_token = "", properties;

        if (type === 0) {
            init_token = MD5.hexdigest(username + lie_code);
        } else if (type === 1) {
            init_token = MD5.hexdigest(username + project + truth_code);
        }

        if (readCookie("rdid") && readCookie("rdid") !== "") {
            properties = [{'key': 'rdid', 'value': readCookie("rdid")}];
        } else {
            properties = [];
        }

        var data = {
            "email": email,
            "name": name,
            "password": password,
            "project": project,
            "token": init_token,
            "type": type,
            "user_id": user_id,
            "username": username,
            "properties": properties
        };

        callApi("https://recommend.zamba.vn/chatApi/restapi/v1/users", JSON.stringify(data), "POST", "JSON", function (response) {
            var result = JSON.parse(response);
            console.log("Created new user successfully: ", result);
            if (result.status === "1") {
                writeCookie("rdid", result.data.rdid, 20 * 365);
                chatId = result.data.chat_id;
                token = result.data.token;
                writeCookie("userChatID", chatId);
                writeCookie("userToken", token);
                checkOnline();
                getSupport();
            } else {

            }
        }, function () {
            console.log("Fail to create user account");
        });
        //}
        return 1;
    }

    function checkOnline() {
        var project = project_name;
        callApi("https://rd.zamba.vn:9090/plugins/onlineservice/v1/support/checkonline", "_project=" + project,
            "POST", "TEXT", function (response) {
                var result = JSON.parse(response);
                console.log("Check online: ", result);
                if (result.content === "online") {
                    document.getElementsByClassName("online-icon")[0].setAttribute("class", "online-icon online");
                } else {
                    document.getElementsByClassName("online-icon")[0].setAttribute("class", "online-icon offline");
                    document.getElementsByClassName("header-title")[0].getElementsByTagName("span")[0].innerHTML = "Tư vấn đang offline";
                    writeCookie("supportName", "");
                }
            }, function () {
                console.log("Fail to check online");
            });
    }

    function getSupport() {
        var jid = readCookie("userChatID"), project = project_name, code = MD5.hexdigest(jid + project);
        var data = {
            jid: jid,
            project: project,
            code: code
        }
        callApi("https://rd.zamba.vn:9090/plugins/onlineservice/v1/support/getsupport", JSON.stringify(data), "POST", "JSON", function (response) {
            var result = JSON.parse(response);
            console.log("Support: ", result);
            if (result.data.name !== readCookie("supportName")) {
                writeCookie("isUpdated", 1);
            }
            writeCookie("supportName", result.data.name);
            writeCookie("supportJID ", result.data.jid);
            if (document.querySelector(".chat-box.show")) {
                document.getElementsByClassName("header-title")[0].getElementsByTagName("span")[0].innerHTML = readCookie("supportName");
            } else {
                document.getElementsByClassName("header-title")[0].getElementsByTagName("span")[0].innerHTML = "Tư" +
                    " vấn đang online";
            }

            var url = "https://rdapi.zamba.vn/static/avatar/" + result.data.avatar;
            writeCookie("supportAva", url);
            var support_jid = document.createElement("input");
            support_jid.setAttribute("type", "hidden");
            support_jid.value = result.data.jid;
            document.getElementsByClassName("header-chat")[0].getElementsByClassName("header-title")[0].appendChild(support_jid);
            document.getElementsByClassName("online-icon")[0].setAttribute("class", "online-icon online");

            document.getElementsByClassName("overlay")[0].style.display = "block";
            getHistory(result.data.jid, url);
            setInterval(function () {
                checkStatus(result.data.jid);
            }, 60000);

            connection = new Strophe.Connection(BOSH_SERVICE);
            connection.connect(chatId + "@" + domain, token, onConnect);
        }, function () {
            console.log("Fail to get support");
        });
    }

    function getHistory(to, url) {
        var noti = document.getElementsByClassName("notify")[0];
        if (msg_num > 0 && document.querySelector(".chat-box.hide") !== null) {
            noti.innerHTML = msg_num;
            noti.style.display = "block";
            document.getElementsByClassName("chat-box")[0].addEventListener("click", function () {
                noti.style.display = "none";
                writeCookie("msg_num" + readCookie("userChatID"), 0);
            });
        }

        var fromChatID = readCookie("userChatID"), toChatID = to + "@" + domain, project = project_name;
        var data = {
            fromChatID: fromChatID,
            toChatID: toChatID,
            project: project
        }

        callApi("https://recommend.zamba.vn/chatApi/rdapi/v1/helper/histories", JSON.stringify(data), "POST", "JSON", function (response) {
            var result = JSON.parse(response);
            console.log("Load history: ", result);
            document.getElementsByClassName("overlay")[0].style.display = "none";
            var messages = result.data;
            var len = messages.length;

            if (to !== readCookie("supportJID")) {
                var historyChat = document.getElementsByClassName("history-chat")[0];
                while (historyChat.firstChild) {
                    historyChat.removeChild(historyChat.firstChild);
                }
            }

            if (len > 0) {
                var fromId = "", msg = "";
                while (len--) {
                    fromId = messages[len].fromChatID;
                    fromId = fromId.slice(0, fromId.indexOf("@"));
                    msg = JSON.parse(messages[len].bodyMsg).msg;
                    if (msg === "") {
                        msg = JSON.parse(messages[len].bodyMsg).images;
                    }
                    appendHistory(fromId, msg, url);
                }
            } else {
                if (!document.querySelector("greeting")) {
                    createMessage("left", "Chào mừng", true, false, url, true);
                }
            }
        }, function () {
            console.log("Fail to get history chat");
        });
    }

    function appendHistory(from, msg, url) {
        if (from === readCookie("userChatID")) {
            createMessage("right", msg, false, false, "", false);
        } else {
            createMessage("left", msg, false, false, url, false);
        }
        document.getElementsByClassName("history-chat")[0].scrollTop = document.getElementsByClassName("history-chat")[0].scrollHeight;
    }

    function checkStatus(chatID) {
        var data = {
            "msg": "Check Status",
            "status": 1,
            "chatID": [chatID]
        };

        callApi("https://rd.zamba.vn:9090/plugins/onlineservice/v1/support/status", JSON.stringify(data), "POST", "JSON", function (response) {
            var result = JSON.parse(response);
            console.log("Check status: ", result);
            if (result.data[0].status === "0") {
                document.getElementsByClassName("online-icon")[0].setAttribute("class", "online-icon offline");
                document.getElementsByClassName("header-title")[0].getElementsByTagName("span")[0].innerHTML = "Tư" +
                    " vấn viên đang offline";
            } else if (result.data[0].status === "1") {
                document.getElementsByClassName("online-icon")[0].setAttribute("class", "online-icon online");
                document.getElementsByClassName("header-title")[0].getElementsByTagName("span")[0].innerHTML = readCookie("supportName");
            }
        }, function () {
            console.log("Fail to check support status");
        });
    };

    function createMessage(pos, content, isGreeting, isNoti, avaUrl, order) {

        var message = document.createElement("li");
        if (isNoti === true) {
            message.innerHTML = content;
            message.setAttribute("class", "message msg-noti");
        } else {
            message.setAttribute("class", "message msg-" + pos);
        }

        var txt = content, msg_img = "";
        if (content.indexOf("<a href='http://chat.zamba.vn/ufile/images.php?size=default&file=") === -1 && content.indexOf(".jpg' target='_blank'><img src='http://chat.zamba.vn/ufile/images.php?size=size01&file=") === -1) {
            var url_regex = /(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/igm;
            while (match = url_regex.exec(content)) {
                var url = content.slice(match.index, url_regex.lastIndex);
                var rightUrl;
                if (url.indexOf("www") === 0) {
                    url = "http://" + url;
                    rightUrl = "<a href='" + url + "'>" + url + "</a>";
                    txt = txt.replace(url.slice(7, url.length), rightUrl);
                } else {
                    rightUrl = "<a href='" + url + "'>" + url + "</a>";
                    txt = txt.replace(url, rightUrl);
                }
            }
        } else {
            msg_img = " msgImg";
        }

        if (pos !== "") {
            var messageContent = document.createElement("div");
            messageContent.setAttribute("class", "msg" + msg_img);
            if (txt.match(url_regex)) {
                messageContent.insertAdjacentHTML("beforeend", txt)
            } else {
                messageContent.innerHTML = content;
            }
            if (pos === "left") {
                var messageAvatar = document.createElement("div");
                messageAvatar.setAttribute("class", "avatar");
                messageAvatar.style.background = "url(" + avaUrl + ") no-repeat";
                messageAvatar.style.backgroundSize = "25px 25px";
                message.appendChild(messageAvatar);
            }
            message.appendChild(messageContent);
        }

        if (isGreeting === true) {
            messageContent.classList += " greeting";
            document.getElementsByClassName("body-chat")[0].appendChild(message);
        } else {
            var historyChat;
            if (document.getElementsByClassName("history-chat")[0]) {
                historyChat = document.getElementsByClassName("history-chat")[0];
            } else {
                historyChat = document.createElement("ul");
                historyChat.setAttribute("class", "history-chat");
                document.getElementsByClassName("body-chat")[0].appendChild(historyChat);
            }
            if (order === true) {
                historyChat.appendChild(message);
            } else {
                historyChat.insertBefore(message, historyChat.firstChild);
            }

            document.getElementsByClassName("history-chat")[0].scrollTop = document.getElementsByClassName("history-chat")[0].scrollHeight;
        }
    }

    function onConnect(status) {
        if (status == Strophe.Status.ERROR) {
            console.log("ERROR");
        } else if (status == Strophe.Status.CONNECTING) {
            console.log("CONNECTING");
        } else if (status == Strophe.Status.CONNFAIL) {
            console.log("CONNFAIL");
        } else if (status == Strophe.Status.AUTHENTICATING) {
            console.log("AUTHENTICATING");
        } else if (status == Strophe.Status.AUTHFAIL) {
            console.log("AUTHFAIL");
        } else if (status == Strophe.Status.DISCONNECTING) {
            console.log("DISCONNECTING");
        } else if (status == Strophe.Status.DISCONNECTED) {
            writeCookie("connected", 0);
            console.log("DISCONNECTED");
            reConnect();
        } else if (status == Strophe.Status.ATTACHED) {
            console.log("ATTACHED");
        } else if (status == Strophe.Status.CONNECTED) {
            console.log("CONNECTED");
            writeCookie("connected", 1);

            connection.addHandler(onMessage, "jabber:client", "message", "chat", null, null, null);
            //connection.addHandler(onForwaredMessage, "urn.xmpp.forward:0", "message", "chat", null, null, null);
            connection.send($pres().c('status'));
        }
    }

    function reConnect() {
        connection.addHandler(onMessage, "jabber:client", "message", "chat", null, null, null);
        //connection.addHandler(onForwaredMessage, "urn.xmpp.forward:0", "message", "chat", null, null, null);
    }

    function onMessage(message) {
        writeCookie("isUpdated", 2);
        var to = message.getAttribute('from');
        var from = message.getAttribute('from');
        var type = message.getAttribute('type');
        var elems = message.getElementsByTagName('body');

        if (type == "chat" && elems.length > 0) {
            var body = elems[0];
            showMessage(JSON.parse(JSON.parse(JSON.stringify(Strophe.getText(body).replace(/&quot;/g, '"')))).msg);
        }
        return true;
    }

    function showMessage(msg) {
        if (document.getElementsByClassName("avatar")[0]) {
            var img = document.getElementsByClassName("avatar")[0];
            var style = img.currentStyle || window.getComputedStyle(img, false);
            var url = style.backgroundImage.slice(4, -1).replace(/"/g, "");
        }

        createMessage("left", msg, false, false, url, true);

        document.getElementsByClassName("history-chat")[0].scrollTop = document.getElementsByClassName("history-chat")[0].scrollHeight;
        var title = document.title, notify = document.getElementsByClassName("notify")[0];
        msg_num++;
        notify.innerHTML = msg_num;

        var refreshId = setInterval(function () {
            document.title = (document.title === title) ? "Bạn có " + msg_num + " tin nhắn mới" : title;
        }, 1000);

        document.getElementsByClassName("chat-box")[0].addEventListener("click", function () {
            document.title = title;
            msg_num = 0;
            writeCookie("msgNum_" + readCookie("userChatID"), msg_num);
            notify.style.display = "none";
            clearInterval(refreshId);
        });

        writeCookie("msgNum_" + readCookie("userChatID"), msg_num);

        if (notify.innerHTML !== "0" && document.querySelector(".chat-box.hide") !== null) {
            notify.style.display = "block";
        } else {
            notify.style.display = "none";
        }
    }

    function connectServer() {
        var script = document.getElementsByTagName('script')[0], js = document.createElement('script');
        js.src = 'strophe.min.js';
        js.onload = function () {
            createUser();
        };
        script.parentNode.insertBefore(js, script);
    }

    function callApi(url, data, method, type, success, error) {
        var xmlhttp;
        if (XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
            if ("withCredentials" in xmlhttp) {
                xmlhttp.open(method, url);
                if (type.toLowerCase() === "text") {
                    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
                } else if (type.toLowerCase() === "json") {
                    xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
                } else if (type.toLowerCase() === "form-data") {
                    xmlhttp.setRequestHeader("Content-type", "multipart/form-data; charset=UTF-8");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState === 4) {
                        if (xmlhttp.status >= 200 && xmlhttp.status < 400) {
                            success(xmlhttp.responseText);
                        } else {
                            error(xmlhttp);
                        }
                    }
                };
                if (method.toLowerCase() === "post") {
                    xmlhttp.send(data);
                } else if (method.toLowerCase() === "get") {
                    xmlhttp.send();
                }
            }
        } else if (XDomainRequest) {
            xmlhttp = new XDomainRequest();
            xmlhttp.open(method, url);
            xmlhttp.onerror = error(xmlhttp);
            xmlhttp.onload = function () {
                success(xmlhttp);
            }
            if (method.toLowerCase() === "post") {
                xmlhttp.send(data);
            } else if (method.toLowerCase() === "get") {
                xmlhttp.send();
            }
        } else {
            errback(new Error("CORS not supported"));
        }
    };

    // Utility method to extend defaults with user options
    function extendDefaults(source, properties) {
        var property;
        for (property in properties) {
            if (properties.hasOwnProperty(property)) {
                source[property] = properties[property];
            }
        }
        return source;
    }
}());

function readCookie(data) {
    var key, temp, cookie, i;
    key = data + "=";
    cookie = document.cookie.split(";");
    for (i = 0; i < cookie.length; i++) {
        temp = cookie[i];
        while (temp.charAt(0) == " ") {
            temp = temp.substring(1, temp.length);
        }
        if (temp.indexOf(key) == 0) {
            return temp.substring(key.length, temp.length);
        }
    }
    return "";
}

var deleteCookie = function (data, path) {
    var domain = document.domain;
    document.cookie = data + "=;path=" + path + ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
};

var writeCookie = function (data, value, time) {
    var date, expires;
    if (time) {
        date = new Date();
        date.setTime(date.getTime() + (time * 24 * 60 * 60 * 1000));
        expires = ";expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = data + "=" + value + expires + ";path=/";
};

var supportChat = new SupportChatBox({
    connectFirst: false
});
supportChat.init();